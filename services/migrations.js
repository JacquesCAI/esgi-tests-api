const DB = require("./DB")

module.exports = () =>
  DB.sync({ alter: true })
    .then(() => {
      console.log("Database synced")
    })
    .catch(err => {
      console.log("Error creating database")
      console.log(err)
    })
