const Sequelize = require("sequelize")

const sequelize = new Sequelize(
  "postgres://root:password@postgres:5432/app",
  { logging: false }
)

try {
  sequelize.authenticate().then(() => {
    console.log("Connection has been established successfully.")
  })
} catch (error) {
  console.error("Unable to connect to the database:", error)
}

module.exports = sequelize
