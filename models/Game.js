"use strict"
const { Model, DataTypes } = require("sequelize")
const sequelize = require("../services/DB")

class Game extends Model {}

Game.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    genres: {
      type: DataTypes.JSON,
      allowNull: true
    },
    console: {
      type: DataTypes.STRING,
      allowNull: false
    }
  },
  {
    sequelize,
    modelName: "Game"
  }
)
module.exports = Game
