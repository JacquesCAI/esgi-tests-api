"use strict"
const { Model, DataTypes } = require("sequelize")
const sequelize = require("../services/DB")

class Console extends Model {
  static associate(models) {
    // define association here
  }
}
Console.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    editor: {
      type: DataTypes.STRING,
      allowNull: false
    },
    releaseDate: {
      type: DataTypes.DATE,
      allowNull: true
    }
  },
  {
    sequelize,
    modelName: "Console"
  }
)

module.exports = Console
