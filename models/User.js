"use strict"
const { Model, DataTypes } = require("sequelize")
const sequelize = require("../services/DB")
const bcrypt = require("bcryptjs")
class User extends Model {
}
User.init(
  {
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    roles: {
      type: DataTypes.JSON,
      allowNull: true
    }
  },
  {
    sequelize,
    modelName: "User"
  }
)
const encodePassword = user => {
  user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10))
}
User.addHook("beforeCreate", encodePassword)
User.addHook("beforeUpdate", encodePassword)
module.exports = User
