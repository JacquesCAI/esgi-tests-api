const app = require("./app")
const migrations = require("./services/migrations")
const port = process.env.PORT || 3000

migrations().then(() => console.log("Migrations done"))

app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})
