const express = require("express")
const {default: crud} = require("express-crud-router");
const {default: sequelizeCrud} = require("express-crud-router-sequelize-v6-connector");
const {Game, Console} = require("./models/index");
const {hasRole,isAuth} = require("./middlewares/security");

const app = express()

app.use(express.json())

app.use("/users", require("./routes/user.route"))
app.use("/consoles", require("./routes/console.route"))
app.use("/games", require("./routes/game.route"))

//app.use(isAuth);
//app.use(hasRole('ADMIN'));
// app.use(crud('/game', sequelizeCrud(Game)));
// app.use(crud('/console', sequelizeCrud(Console)));

module.exports = app
