const request = require("supertest")
const DB = require("../../services/DB")
const migrations = require("../../services/migrations");

let t;

beforeAll(() => {
  return migrations();
});

beforeEach(async () => {
  t = await DB.transaction();
  DB.constructor['_cls'] = new Map();
  DB.constructor['_cls'].set('transaction', t);
})

afterAll(() => {
  return DB.close();
});


afterEach(() => {
  return t.rollback();
});

describe("Game", () => {
  it("should return all game", async () => {
    const app = require("../../app");
    const client = request(app);
    const res = await client.get("/games");
    expect(res.status).toBe(200);
    expect(res.body.length).toBe(0);
  })
  it("Create one game, should return one game", async () => {
    const game = {
      name: "The Elder Scroll VI",
      price: 70,
      genres: ['Fantastique', 'Action','Ne sortira jamais'],
      console: "PS12"
    }

    const app = require("../../app");
    const client = request(app);
    const resPost = await client
        .post("/games/")
        .send(game)
    const postResJson = JSON.parse(resPost.text);
    expect(resPost.status).toBe(201);
    expect(postResJson).toEqual(expect.objectContaining(game))

    const resGet = await client
        .get("/games/"+postResJson.id);
    expect(resGet.status).toBe(200);
    expect(JSON.parse(resGet.text)).toEqual(expect.objectContaining(game))
  })
});
