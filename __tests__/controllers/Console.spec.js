const request = require("supertest")
const DB = require("../../services/DB")
const migrations = require("../../services/migrations");

let t;

beforeAll(() => {
  return migrations();
});

beforeEach(async () => {
  t = await DB.transaction();
  DB.constructor['_cls'] = new Map();
  DB.constructor['_cls'].set('transaction', t);
})

afterAll(() => {
  return DB.close();
});

afterEach(() => {
  return t.rollback();
});

describe("Console", () => {
  it("should return all console", async () => {
    const app = require("../../app");
    const client = request(app);
    const res = await client.get("/consoles");
    expect(res.status).toBe(200);
    expect(res.body.length).toBe(0);
  })
  it("Create one console, should return one console", async () => {
    const console = {
      name: "PS12",
      editor: "Sony",
      releaseDate: new Date('12-25-2050').toISOString()
    }

    const app = require("../../app");
    const client = request(app);
    const resPost = await client
        .post("/consoles/")
        .send(console)
    const postResJson = JSON.parse(resPost.text);
    expect(resPost.status).toBe(201);
    expect(postResJson).toEqual(expect.objectContaining(console))

    const resGet = await client
        .get("/consoles/"+postResJson.id);
    expect(resGet.status).toBe(200);
    expect(JSON.parse(resGet.text)).toEqual(expect.objectContaining(console))
  })
});
