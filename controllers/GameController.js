module.exports = function ConsoleController(Game) {
  return {
    getOne: async (req, res) => {
      const game = await Game.findByPk(req.params.id)
      if (!game) {
        return res.sendStatus(404)
      }
      res.send(game)
    },
    getAll: async (req, res) => {
      const games = await Game.findAll()
      res.json(games)
    },
    create: function(req, res) {
      new Game(req.body)
        .save()
        .then(data => res.status(201).json(data))
        .catch(e => {
          if (e.name === "SequelizeValidationError") {
            res.status(400).json(e.errors)
          } else {
            res.sendStatus(500)
          }
        })
    },
    update: function(req, res) {
      const { id } = req.params
      Game.update(req.body, {
        where: { id },
        returning: true,
        individualHooks: true
      })
        .then(
          ([, [data]]) =>
            data !== undefined
              ? res.status(200).json(data)
              : res.sendStatus(404)
        )
        .catch(e => {
          if (e.name === "SequelizeValidationError") {
            res.status(400).json(e.errors)
          } else {
            res.sendStatus(500)
          }
        })
    },
    delete: function(req, res) {
      const { id } = req.params
      Game.destroy({
        where: { id }
      })
        .then(() => res.sendStatus(204))
        .catch(err => res.sendStatus(500))
    }
  }
}
