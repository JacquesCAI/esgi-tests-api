const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")

module.exports = function UserController(User) {
  return {
    getOne: async (req, res) => {
      const user = await User.findByPk(req.params.id)
      if (!user) {
        return res.status(404).send("User not found")
      }
      res.send(user)
    },
    getAll: async (req, res) => {
      const users = await User.findAll()
      res.json(users)
    },
    register: async (req, res) => {
      User.create({
        ...req.body
      })
        .then(user => {
          delete user.password
          res.status(201).send(user)
        })
        .catch(err => {
          res.status(500).send({ message: err })
        })
    },
    login: async (req, res) => {
      User.findOne({
        where: {
          username: req.body.username
        }
      })
        .then(async user => {
          if (!user) {
            return res.sendStatus(404)
          }
          bcrypt.compare(req.body.password, user.password, (err, response) => {
            if (err) {
              throw new Error(err)
            }
            if (response) {
              const expireIn = "24h"
              const token = jwt.sign(
                {
                  ...user.dataValues,
                  password: undefined
                },
                process.env.SECRET_KEY,
                { expiresIn: expireIn }
              )
              return res.status(200).send({
                message: "Authentification succesfull",
                token
              })
            }
            return res.sendStatus(401)
          })
        })
        .catch(err => {
          res.status(500).send({ message: err })
        })
    }
  }
}
