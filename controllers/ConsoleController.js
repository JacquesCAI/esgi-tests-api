module.exports = function ConsoleController(Console) {
  return {
    getOne: async (req, res) => {
      const console = await Console.findByPk(req.params.id)
      if (!console) {
        return res.sendStatus(404)
      }
      res.send(console)
    },
    getAll: async (req, res) => {
      const consoles = await Console.findAll()
      res.json(consoles)
    },
    create: function(req, res) {
      new Console(req.body)
        .save()
        .then(data => res.status(201).json(data))
        .catch(e => {
          if (e.name === "SequelizeValidationError") {
            console.error(e)
            res.status(400).json(e.errors)
          } else {
            console.log(e)
            res.sendStatus(500)
          }
        })
    },
    update: function(req, res) {
      const { id } = req.params
      Console.update(req.body, {
        where: { id },
        returning: true,
        individualHooks: true
      })
        .then(
          ([, [data]]) =>
            data !== undefined
              ? res.status(200).json(data)
              : res.sendStatus(404)
        )
        .catch(e => {
          if (e.name === "SequelizeValidationError") {
            res.status(400).json(e.errors)
          } else {
            res.sendStatus(500)
          }
        })
    },
    delete: function(req, res) {
      const { id } = req.params
      Console.destroy({ where: { id } })
        .then(data => (data === 0 ? res.sendStatus(404) : res.sendStatus(204)))
        .catch(e => res.sendStatus(500))
    }
  }
}
