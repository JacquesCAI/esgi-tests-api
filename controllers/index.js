const { User, Game, Console } = require("../models")

exports.UserController = require("./UserController")(User)
exports.ConsoleController = require("./ConsoleController")(Console)
exports.GameController = require("./GameController")(Game)