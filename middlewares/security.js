const jwt = require("jsonwebtoken");
module.exports = {
    isAuth: function isAuth(req,res,next) {
        const token = req.headers.authorization ? req.headers.authorization.split(" ")[1] : null

        if (!token)
            return res.sendStatus(401);

        jwt.verify(token, process.env.SECRET_KEY, (err, user) => {
            if (err) {
                console.error(err);
                return res.sendStatus(500)
            }
            req.user = user;
            next();
        })
    },
    hasRole: role => (req,res,next) => {
        if (!req.user)
            return res.sendStatus(401);

        if (!req.user.roles || !req.user.roles.includes(role))
            return res.sendStatus(403)

        next();
    }
}