const { Router } = require("express")
const router = new Router()
const { UserController } = require("../controllers")

router.get("/", UserController.getAll)
router.get("/:id", UserController.getOne)
router.post("/register", UserController.register)
router.post("/login", UserController.login)

module.exports = router
