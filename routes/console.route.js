const { Router } = require("express")
const router = new Router()
const { ConsoleController } = require("../controllers")

router.get("/", ConsoleController.getAll)
router.get("/:id", ConsoleController.getOne)
router.post("/", ConsoleController.create)
router.put("/:id", ConsoleController.update)
router.delete("/:id", ConsoleController.delete)

module.exports = router