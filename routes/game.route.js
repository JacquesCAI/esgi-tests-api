const { Router } = require("express")
const router = new Router()
const { GameController } = require("../controllers")

router.get("/", GameController.getAll)
router.get("/:id", GameController.getOne)
router.post("/", GameController.create)
router.put("/:id", GameController.update)
router.delete("/:id", GameController.delete)

module.exports = router